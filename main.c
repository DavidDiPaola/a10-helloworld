typedef unsigned int u32;

void
main(void) {
	/* LED is port H 2 */
	#define pio_BASE ((void *)0x01C20800)
	volatile u32 * const pio_porth_CONFIG = (pio_BASE + 0x0FC);  /* aka PH_CFG0 */
		const u32 pio_porth_CONFIG_pin2_MASK   = (0b111 << 8);
		const u32 pio_porth_CONFIG_pin2_OUTPUT = (0b001 << 8);
	volatile u32 * const pio_porth_DATA   = (pio_BASE + 0x10C);  /* aka PH_DAT */
		const u32 pio_porth_DATA_pin2_ENABLE = (0b1 << 2);
	
	u32 config = (*pio_porth_CONFIG);
	config &= ~(pio_porth_CONFIG_pin2_MASK  );
	config |=  (pio_porth_CONFIG_pin2_OUTPUT);
	(*pio_porth_CONFIG) = config;

	for (;;) {
		(*pio_porth_DATA) ^= pio_porth_DATA_pin2_ENABLE;
		for (volatile u32 i=0; i<(1 << 28); i++) {}
	}
}

