SRC = main.c
BIN_ELF = boot.elf
BIN_BIN = $(BIN_ELF:.elf=.bin)

OBJ = $(SRC:.c=.o)

PREFIX ?= arm-none-eabi-
GCC ?= $(PREFIX)gcc
GCC_FLAGS = \
	-std=c99 \
	-mcpu=cortex-a8 \
	-ffreestanding -nostdinc \
	-ffunction-sections -fdata-sections \
	-O2 \
	-g \
	$(CPP_FLAGS_DEBUG)
GLD ?= $(PREFIX)ld
GLD_FLAGS = \
	-static -nostdlib \
	-O1 --gc-sections --print-gc-sections
OBJCOPY ?= $(PREFIX)objcopy
OBJDUMP ?= $(PREFIX)objdump
OBJDUMP_FLAGS = \
	--disassemble
MKIMAGE ?= mkimage
SUNXIFEL ?= sunxi-fel

.PHONY: all
all: $(BIN_BIN) boot.scr

.PHONY: dump-elf
dump-elf: $(BIN_ELF)
	$(OBJDUMP) $(OBJDUMP_FLAGS) --source --line-numbers $<

.PHONY: fel
fel: u-boot-sunxi-with-spl.bin $(BIN_BIN) boot.scr
	$(SUNXIFEL) uboot u-boot-sunxi-with-spl.bin \
		write 0x40000000 $(BIN_BIN) \
		write 0x43100000 boot.scr

.PHONY: clean
clean:
	rm -rf $(OBJ) $(BIN_ELF) $(BIN_BIN) uImage boot.scr

.c.o:
	$(GCC) $(GCC_FLAGS) -o $@ -c $<

$(BIN_ELF): layout.lds $(OBJ)
	@# TODO figure out why omitting `--gc-sections` causes overlap errors
	$(GLD) $(GLD_FLAGS) -T layout.lds -o $@ $(OBJ)

$(BIN_BIN): $(BIN_ELF)
	$(OBJCOPY) -O binary $< $@

boot.scr: boot.cmd
	$(MKIMAGE) -A arm -T script -C none -d $< $@

